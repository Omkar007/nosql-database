#pragma once
/////////////////////////////////////////////////////////////////////
// NoSqlDb.h - key/value pair in-memory database                   //
//                                                                 //
// Omkar Patil, CSE687 - Object Oriented Design, Spring 2017       //
/////////////////////////////////////////////////////////////////////
/*
* - Manual Information
	This package provides the Template class for the NoSQL DB database 
	and also defines the Template class structure for the DBElement to 
	be stored in the database. It provides fucntionalities for add/delete
	and editing key value pairs as follows.
	Public Interface:
	=================
	std::unordered_map<std::string, DBElement<Data>>& getInMemoryDatabase() { return hashTable; }
	bool save(std::string key, DBElement<Data> elem);
	bool deletebyKey(std::string key);
	bool editbyKey(std::string key, DBElement<Data> elem);
	bool editMetadata(std::string key, std::string edtName, std::string edtCategory,Data edtdata);
	bool editChildKeys(std::string key, std::string funcParam, std::string editPos, std::string editValue);

* - Query mechanism
	Support the functionalities so as user can query database and get the desired results. 
	 Public Interface:
	=================
	DBElement<Data> value(std::string key);
	std::vector<std::string> NoSqlDb<Data>::querychildKeys(std::string key);
	std::vector<std::string> NoSqlDb<Data>::getKeysfromPattern(std::vector<std::string>& keySets,std::string pattern);
	std::vector<std::string> NoSqlDb<Data>::queryItemKeys(std::vector<std::string>& keySets,std::string pattern);
	std::vector<std::string> NoSqlDb<Data>::queryCategoryKeys(std::vector<std::string>& keySets,std::string pattern);
	std::vector<std::string> NoSqlDb<Data>::queryDataPatternKeys(std::vector<std::string>& keySets,std::string pattern);
	std::vector<std::string> NoSqlDb<Data>::CompoundQuery(bool type, std::vector<std::string>& param1, std::vector<std::string>& param2);
	std::vector<std::string> NoSqlDb<Data>::queryTimeDateKeys(std::vector<std::string>& keySets, std::string startDate, std::string endDate);

* - Persistance mechanism
	Uses Package XML document which persist data to xml and load data to database.
* - TestExecutive that demonstrates the requirements you meet.

* - Build Process:
	Required files
	- CppProperties.h, NoSqlDb.h,NoSqlDb.cpp
	Build commands (either one)
	- devenv NOSql.sln

* - Maintenance information
ver 1.0 : 07 Feb 2017
-	first release
*/


#include <unordered_map>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <regex>
#include <set>
#include "CppProperties\CppProperties.h"


// Template Class to define the Data and its Metadata  to be stored in NoSql Database
template<typename Data>
class DBElement
{
private:

	std::string name;         // metadata
	std::string category;    // metadata
	std::string timeDate;    // metadata
	Data data;				 // data
	std::vector<std::string> childKeys;

public:
	std::string show();
	DBElement() {}
	std::string getName() const { return name; }
	std::string getCategory() const { return category; }
	std::string gettimeDate() const { return timeDate; }
	Data getData() const { return data; }
	std::vector<std::string>& getchildKeys() { return childKeys; }
	void setName(std::string& _name) {name = _name; }
	void setCategory(std::string& _category) {category=_category; }
	void setData(Data& _data) {data=_data; }
	void setTimeDate(std::string& _timeDate) { timeDate = _timeDate; }
	friend std::ostream& operator << (std::ostream &out, std::vector<int> &tm);
	friend std::istream& operator >> (std::istream  &input, std::vector<int> &tm);

	DBElement(std::string nameArg, std::string catArg, std::string timeArg, Data dataArg) {
		name = nameArg;
		category = catArg;
		timeDate = timeArg;
		data = dataArg;
	}
};

//Overloaded output operator to support output operation for vector<int>
std::ostream& operator<< (std::ostream &out, std::vector<int> &tm)
{
	for (std::vector<int>::size_type i = 0; i != tm.size(); i++) {
		out << tm[i];
	}
	return out;
}

//Overloaded input operator to support input operation for vector<int>
std::istream& operator >> (std::istream  &input,std::vector<int> &tm) {
	std::string str;
	input >> str;
	tm.push_back(std::stoi(str));
	return input;
}

//To display the Data and it's Metadata of a particular key
template<typename Data>std::string DBElement<Data>::show()
{
	std::ostringstream out;
	out.setf(std::ios::adjustfield, std::ios::left);
	out << "\n    " << std::setw(8) << "name" << " : " << name;
	out << "\n    " << std::setw(8) << "category" << " : " << category;
	out << "\n    " << std::setw(8) << "timeDate" << " : " << timeDate;
	out << "\n    " << std::setw(8) << "data" << " : " << data;
	out << "\n    " << std::setw(8) << "child dependencies" << " : ";
	for (std::vector<std::string>::iterator it=childKeys.begin();it<childKeys.end();it++)
	{
		out << std::setw(8) <<*it;
	}
	out << "\n";
	return out.str();
}

// Tempate class (In Memory Database) to store the key/value pair in the Memory
template<typename Data>
class NoSqlDb
{
public:
	using Keys = std::vector<std::string>;
	Keys keys();
	std::unordered_map<std::string, DBElement<Data>>& getInMemoryDatabase() { return hashTable; }
	bool save(std::string key, DBElement<Data> elem);
	bool deletebyKey(std::string key);
	bool editbyKey(std::string key, DBElement<Data> elem);
	bool editMetadata(std::string key, std::string edtName, std::string edtCategory,Data edtdata);
	bool editChildKeys(std::string key, std::string funcParam, std::string editPos, std::string editValue);
	size_t count();

	DBElement<Data> value(std::string key);
	std::vector<std::string> NoSqlDb<Data>::querychildKeys(std::string key);
	std::vector<std::string> NoSqlDb<Data>::getKeysfromPattern(std::vector<std::string>& keySets,std::string pattern);
	std::vector<std::string> NoSqlDb<Data>::queryItemKeys(std::vector<std::string>& keySets,std::string pattern);
	std::vector<std::string> NoSqlDb<Data>::queryCategoryKeys(std::vector<std::string>& keySets,std::string pattern);
	std::vector<std::string> NoSqlDb<Data>::queryTimeDateKeys(std::vector<std::string>& keySets, std::string startDate, std::string endDate);
	std::vector<std::string> NoSqlDb<Data>::queryDataPatternKeys(std::vector<std::string>& keySets,std::string pattern);
	std::vector<std::string> NoSqlDb<Data>::CompoundQuery(bool type, std::vector<std::string>& param1, std::vector<std::string>& param2);

private:
	using Item = std::pair<std::string, DBElement<Data>>;		// Defining pair for iterator to retrieve elements in key / value form.
	std::unordered_map<std::string, DBElement<Data>> hashTable;	// In memory data structure as hash table to store the data and its metadata as key value pair.
};

// Function used to retrieve all keys saved in hashtable
template<typename Data>
typename NoSqlDb<Data>::Keys NoSqlDb<Data>::keys()		
{	Keys keys;
	for (Item item : hashTable)
	{
		keys.push_back(item.first);						//stores keys one by one
	}
	return keys;
}

// Function to add key/Value pair in InMemory database 
template<typename Data>										
bool NoSqlDb<Data>::save(std::string key, DBElement<Data> elem)	 
{
	try {
		if (hashTable.find(key) != hashTable.end())
			return false;
		hashTable[key] = elem;
		return true;
	}
	catch (std::exception& ex)
	{
		std::cout << "\n  Exception occured in saving element to hashtable";
		std::cout << "\n  " << ex.what();
		return false;
	}
}

// Function to delete key/Value pair by key
template<typename Data>
bool NoSqlDb<Data>::deletebyKey(std::string key) {				
	try {
		if (hashTable.find(key) != hashTable.end()) {
			hashTable.erase(key);
			return true;
		}
	}
	catch (std::exception& ex)
	{	std::cout << "\n  Exception occured in deleting element by key from hashtable";
		std::cout << "\n  " << ex.what();
	}
	return false;
}

// Function to replace an instance of value with new instance on basis of key
template<typename Data>
bool NoSqlDb<Data>::editbyKey(std::string key, DBElement<Data> elem) {
	try {
		if (hashTable.find(key) != hashTable.end()) {
			hashTable[key] = elem;
			return true;
		}
	}
	catch (std::exception& ex)
	{
	std::cout << "\n  Exception occured in editing element's value using key from hashtable";
	std::cout << "\n  " << ex.what();
	}		
	return false;	
}

// Function to edit element's Metadata using key as input param
template<typename Data>
bool NoSqlDb<Data>::editMetadata(std::string key, std::string edtName, std::string edtCategory, Data edtdata) {
	try {
		if (hashTable.find(key) != hashTable.end()) {
			DBElement<Data> elem = hashTable[key];
			elem.setName(edtName);
			elem.setCategory(edtCategory);
			elem.setData(edtdata);
			hashTable[key] = elem;
			return true;
		}
	}
	catch (std::exception& ex)
	{	std::cout << "\n  Exception occured in editing element's Metadata using key from hashtable";
		std::cout << "\n  " << ex.what();
	}
	return false;
}

// Function to Add/Delete/Edit element's child Relationships of a specific key
template<typename Data> bool NoSqlDb<Data>::editChildKeys(std::string key,std::string funcParam, std::string editPos, std::string editValue) {
		if (hashTable.find(key) != hashTable.end()) {
			DBElement<Data> elem = hashTable[key];
			if (funcParam=="delete") {
				elem.getchildKeys().erase(std::find(elem.getchildKeys().begin(), elem.getchildKeys().end(),editPos));
			}
			else if (funcParam == "add")
			{
				elem.getchildKeys().push_back(editValue);
			}
			else if(funcParam == "edit"){
				if (hashTable.find(editValue)!= hashTable.end()){
					std::replace(elem.getchildKeys().begin(), elem.getchildKeys().end(),editPos,editValue);
				}
			}
			hashTable[key] = elem;
			return true;
		}
	return false;
}
// To retrieve value of a specified key
template<typename Data>
DBElement<Data> NoSqlDb<Data>::value(std::string key)			
{
	if (hashTable.find(key) != hashTable.end())
		return hashTable[key];
	return DBElement<Data>();
}

// To retrieve child keys of a specified key
template<typename Data>
std::vector<std::string> NoSqlDb<Data>::querychildKeys(std::string key)			
{	std::vector<std::string> childs;
	if (hashTable.find(key) != hashTable.end())
	{
		DBElement<Data> elem = hashTable[key];
		childs=elem.getchildKeys();
	}
	return childs;
}

// To retrieve keys matching a default pattern of a key  using find & regex
template<typename Data>
std::vector<std::string> NoSqlDb<Data>::getKeysfromPattern(std::vector<std::string>& keySets,std::string pattern) {
	std::vector<std::string> patternMatchedkeys;
	std::regex e("^.*" + pattern + ".*$");	//regex to patch the specified pattern from user
	for (std::vector<std::string>::iterator iter = keySets.begin(); iter != keySets.end(); ++iter)
	{	std::string elemKey= *iter;
		if (((elemKey.find(pattern))!=elemKey.npos ) && (std::regex_match(elemKey, e))) {
			patternMatchedkeys.push_back(*iter);
		}
	}
	if (patternMatchedkeys.empty()) { return keys(); }
	else {
		return patternMatchedkeys;
	}
}

// To retrieve keys matching a specified pattern in Item Name & if no such element then returns all keys in db
template<typename Data>
std::vector<std::string> NoSqlDb<Data>::queryItemKeys(std::vector<std::string>& keySets,std::string pattern)
{	std::vector<std::string> keyResults;
for (std::vector<std::string>::iterator iter = keySets.begin(); iter != keySets.end(); ++iter)
{
	std::regex e("^.*"+pattern+".*$");	//regex to patch the specified pattern from user
	std::string elemKey = *iter;
		DBElement<Data>elem = hashTable[elemKey];
		if (((elem.getName().find(pattern))!= elem.getName().npos) && (std::regex_match(elem.getName(), e))) {
			keyResults.push_back(*iter);
		}
}
	if (keyResults.empty()) {return keys();}
	else {
		return keyResults;
	}
}

// To retrieve keys matching a specified pattern in Category Name & if no such element then returns all keys in db
template<typename Data>
std::vector<std::string> NoSqlDb<Data>::queryCategoryKeys(std::vector<std::string>& keySets,std::string pattern)
{
	std::regex e("^.*"+pattern+".*$");		//regex to patch the specified pattern from user
	std::vector<std::string> categoryResults;
	for (std::vector<std::string>::iterator iter = keySets.begin(); iter != keySets.end(); ++iter)
	{
		DBElement<Data>elem = hashTable[*iter];
		if (((elem.getCategory().find(pattern))!= elem.getCategory().npos) && (std::regex_match(elem.getCategory(), e))) {
			categoryResults.push_back(*iter);
		}
	}
	if (categoryResults.empty()) { return keys(); }
	else {
		return categoryResults;
	}
}

//To retrieve keys within timedate interval provided by user & if none present return all set of keys
template<typename Data>
std::vector<std::string> NoSqlDb<Data>::queryTimeDateKeys(std::vector<std::string>& keySets, std::string startDate, std::string endDate) {
	std::vector<std::string> timeKeys;
	for (std::vector<std::string>::iterator iter = keySets.begin(); iter != keySets.end(); ++iter)
	{
		DBElement<std::string>elem = hashTable[*iter];
		if(endDate==""){
			if (elem.gettimeDate()>startDate && elem.gettimeDate()<getTime()) {
				timeKeys.push_back(*iter);
			}
		}
		else {if (elem.gettimeDate()>startDate && elem.gettimeDate()<endDate) {
				timeKeys.push_back(*iter);
			}
		}
	}
	if (timeKeys.empty()) { return keys(); }
	else {
		return timeKeys;
	}
}

// To retrieve keys matching a specified string in template Data of all elements & if no such element then returns all keys in db
template<typename Data>
std::vector<std::string> NoSqlDb<Data>::queryDataPatternKeys(std::vector<std::string>& keySets,std::string pattern)
{
	std::regex e("^.*" + pattern + ".*$");	//regex to patch the specified pattern from user
	std::vector<std::string> dataResults;
	for (std::vector<std::string>::iterator iter = keySets.begin(); iter != keySets.end(); ++iter)
	{	DBElement<std::string>elem = hashTable[*iter];
		if (((elem.getData().find(pattern))!=elem.getData().npos) && (std::regex_match(elem.getData(), e))) {
			dataResults.push_back(*iter);
		}
	}
	if (dataResults.empty()) { return keys(); }
	else {
		return dataResults;
	}
}

//To find the union and intersection quries on the set of keys returned from above queries
template<typename Data>
std::vector<std::string> NoSqlDb<Data>::CompoundQuery(bool type, std::vector<std::string>& param1, std::vector<std::string>& param2) {
	std::vector<std::string> compoundResults;
	std::set<std::string> set1(param1.begin(), param1.end());
	std::set<std::string> set2(param2.begin(), param2.end());

	if (type==true) {			// Union operation
		set_union(set1.begin(),set1.end(),set2.begin(),set2.end(),std::back_inserter(compoundResults));
	}
	else {						//Intersection operation
		set_intersection(set1.begin(), set1.end(), set2.begin(), set2.end(), std::back_inserter(compoundResults));
	}
	if (compoundResults.empty()) { return keys(); }	//if no results found then return all keys in database
	else {
		return compoundResults;
	}
}

//Function to get the no of entries in InMemory Database
template<typename Data>
size_t NoSqlDb<Data>::count()							
{
	return hashTable.size();
}




