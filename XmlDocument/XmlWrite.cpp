/////////////////////////////////////////////////////////////////////
// NoSqlDb.cpp - key/value pair in-memory database                 //
//                                                                 //
// Omkar Patil, CSE687 - Object Oriented Design, Spring 2017       //
/////////////////////////////////////////////////////////////////////

#include "../XmlDocument/XmlElement/XmlElement.h"
#include "../CppProperties/CppProperties.h"
#include "../Convert/Convert.h"
#include "../StrHelper.h"
#include "../NoSqlDb.h"
#include "../XmlDocument/XmlWriter.h"
using namespace std;

#ifdef TEST_XMLWriter
	//Test Stub
int main() {
	cout << "Testing xml persists";
	DBElement<string> elm{"elem1","test","12-09-2017:11:09:45","I m here"};
	NoSqlDb<std::string> db;
	db.save("key1",elm);
	XmlParserDB<string> xml;
	string xmldat=xml.persistToDB(db);
	cout << xmldat;
	xml.LoadFromDB(xmldat);
	cin.get();

}
#endif
