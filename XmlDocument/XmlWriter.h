#pragma once
/////////////////////////////////////////////////////////////////////
// XmlWriter.h - Persist objects to XML file					   //
//                                                                 //
// Omkar Patil, CSE687 - Object Oriented Design, Spring 2017       //
/////////////////////////////////////////////////////////////////////
/*
* This package provides template class defining functions to persist
  data to the xml file and load the data from the xml file. It also defines 
  the Project Structure and dependency relationship in th form of XML. 
*
* Package operations:
* -------------------
* This package contains:
	Public Interface:
	=================
* - std::string persistToDB(NoSqlDb<Data>& db);
	NoSqlDb<Data> LoadFromDB(std::string& xml);
	std::string XmlProjectStruct();
	XmlParserDB<Data> LoadProjectStruct(std::string& xml);
	std::string Display();
	void StoreResult(std::vector<SPtr> desc, std::vector<std::string>& vecRes);
*
* Required Files:
* ---------------
*   XmlWriter.h, XmlWrite.cpp,
*   Convert.h, Convert.cpp,
*   CppProperties.h, CppProperties.cpp,
*   XmlDocument.h, XmlDocument.cpp, XmlElement.h, XmlElement.cpp,
*  ---- required to build XmlDocument from file or string ----
*   XmlParser.h, XmlParser.cpp,
*   XmlElementParts.h, XmlElementParts.cpp,
*   ITokenizer.h, Tokenizer.h, Tokenizer.cpp,
*
* Build Instructions:
* -------------------
* - Uses XmlDocument, so build XmlDocument project as static library
*   after undefining its test stub
*
*  Version Release:
* -------------------
*	Ver 1.0 
*	07-02-2017
*
*/
#include "../XmlDocument/XmlDocument/XmlDocument.h"
#include "../XmlDocument/XmlElement/XmlElement.h"
#include "../CppProperties/CppProperties.h"
#include "../Convert/Convert.h"
#include "../StrHelper.h"
#include "../NoSqlDb.h"

using namespace XmlProcessing;

//Template class to persist database of any type to the xml file & load it back to inmemory database
template<typename Data>
class XmlParserDB {
	using SPtr = std::shared_ptr<AbstractXmlElement>;
public:
	std::string PersistToDB(NoSqlDb<Data>& db);
	NoSqlDb<Data> LoadFromDB(std::string& xml);
	std::string XmlProjectStruct();
	XmlParserDB<Data> LoadProjectStruct(std::string& xml);
	std::string Display();
	void StoreResult(std::vector<SPtr> desc, std::vector<std::string>& vecRes);
	std::vector<std::string>& getconvertPackage() { return convertPackage; }
	std::vector<std::string>& getcppPropertyPakage() { return cppPropertyPakage; }
	std::vector<std::string>& getxmlDocPackage() { return xmlDocPackage; }
	std::vector<std::string>& getnoSQLPackage() { return noSQLPackage; }
	std::vector<std::string>& getchildReferences() { return childReferences; }
	friend std::ostream& operator << (std::ostream &out, std::vector<std::string> &tm);
	friend std::istream& operator >> (std::istream  &input, std::vector<std::string> &tm);

private:
	std::vector<std::string> convertPackage;
	std::vector<std::string> cppPropertyPakage;
	std::vector<std::string> xmlDocPackage;
	std::vector<std::string> noSQLPackage;
	std::vector<std::string> childReferences;
};

//function to display project structure details after loading them from XML
template<typename Data>std::string XmlParserDB<Data>::Display()
{
	std::ostringstream out;
	out.setf(std::ios::adjustfield, std::ios::left);
	out << "\n    " << std::setw(8) << "Convert Package" << " : ";
	for (std::vector<std::string>::iterator it = convertPackage.begin(); it<convertPackage.end(); it++)
	{out << std::setw(8) << *it;}
	out << "\n    " << std::setw(8) << "Cpp Properties Package" << " : ";
	for (std::vector<std::string>::iterator it = cppPropertyPakage.begin(); it<cppPropertyPakage.end(); it++)
	{out << std::setw(8) << *it;}
	out << "\n    " << std::setw(8) << "XML Document Package" << " : ";
	for (std::vector<std::string>::iterator it = xmlDocPackage.begin(); it<xmlDocPackage.end(); it++)
	{out << std::setw(8) << *it;}
	out << "\n    " << std::setw(8) << "NOSQL Package" << " : ";
	for (std::vector<std::string>::iterator it = noSQLPackage.begin(); it<noSQLPackage.end(); it++)
	{out << std::setw(8) << *it;}
	out << "\n    " << std::setw(8) << "Children References" << " : ";
	for (std::vector<std::string>::iterator it = childReferences.begin(); it<childReferences.end(); it++)
	{out << std::setw(8) << *it;}
	out << "\n";
	return out.str();
}

//Overloaded output operator to support output operation for vector<std::string>
std::ostream& operator<< (std::ostream &out, std::vector<std::string> &tm)
{
	for (std::vector<std::string>::size_type i = 0; i != tm.size(); i++) {
		out << tm[i];
	}
	return out;
}

//Overloaded input operator to support input operation for vector<std::string>
std::istream& operator >> (std::istream  &input, std::vector<std::string> &tm) {
	std::string str;
	input >> str;
	tm.push_back(str);
	return input;
}
//To Convert the database elements in the xml format to persist them in xml
template<typename Data> std::string XmlParserDB<Data>::PersistToDB(NoSqlDb<Data>& db)
{	std::string xml;XmlDocument doc;
	SPtr pRoot1 = makeTaggedElement("NoSQL Database");
	doc.docElement() = pRoot1;
	for (std::pair<std::string,DBElement<Data>> item : db.getInMemoryDatabase()) {
		DBElement<Data>x = item.second;
		SPtr pRoot = makeTaggedElement("Record");
		pRoot1->addChild(pRoot);
		SPtr pNameElem0 = makeTaggedElement("key");
		pRoot->addChild(pNameElem0);
		SPtr pTextElem0 = makeTextElement(item.first);
		pNameElem0->addChild(pTextElem0);
		SPtr pNameElem1 = makeTaggedElement("name");
		pRoot->addChild(pNameElem1);
		SPtr pTextElem1 = makeTextElement(x.getName());
		pNameElem1->addChild(pTextElem1);
		SPtr pNameElem2 = makeTaggedElement("category");
		pRoot->addChild(pNameElem2);
		SPtr pTextElem2 = makeTextElement(x.getCategory());
		pNameElem2->addChild(pTextElem2);
		SPtr pNameElem3 = makeTaggedElement("timestamp");
		pRoot->addChild(pNameElem3);
		SPtr pTextElem3 = makeTextElement(x.gettimeDate());
		pNameElem3->addChild(pTextElem3);
		SPtr pNameElem6 = makeTaggedElement("childRelationships");
		pRoot->addChild(pNameElem6);
		for (std::vector<std::string>::iterator iter = x.getchildKeys().begin(); iter != x.getchildKeys().end(); ++iter) {
			SPtr pTextElem6 = makeTextElement(*iter);
			pNameElem6->addChild(pTextElem6);
		}
		SPtr pNameElem4 = makeTaggedElement("data");
		pRoot->addChild(pNameElem4);
		Data convertedData = x.getData();
		std::string dataContent = Convert<Data>::toString(convertedData);
		SPtr pTextElem4 = makeTextElement(dataContent);
		pNameElem4->addChild(pTextElem4);}
	xml = doc.toString();
	return xml;
}

//Loading Records from XML file to Inmemory Database
template<typename Data>NoSqlDb<Data> XmlParserDB<Data>::LoadFromDB(std::string& xml)
{	NoSqlDb<Data> databaseFromXml;
	try {XmlDocument doc(xml, XmlDocument::str);
		std::vector<SPtr> desc = doc.descendents("Record").select();
		for (std::size_t i = 0; i < desc.size(); i++) {
			DBElement<Data> elem;
			desc= doc.descendents("key").select();
			std::string key_val = desc[i]->children()[0]->value(); key_val = trim(key_val);
			desc= doc.descendents("name").select();
			std::string name_value = desc[i]->children()[0]->value(); name_value = trim(name_value);
			elem.setName(name_value);
			desc = doc.descendents("category").select();
			std::string cat_value = desc[i]->children()[0]->value(); cat_value = trim(cat_value);
			elem.setCategory(cat_value);
			desc = doc.descendents("timestamp").select();
			std::string time_value = desc[i]->children()[0]->value(); time_value = trim(time_value);
			elem.setTimeDate(time_value);
			desc = doc.descendents("data").select();
			std::string data_value = desc[i]->children()[0]->value();
			elem.setData(Convert<Data>::fromString(data_value));
			desc = doc.descendents("childRelationships").select();
			if(!desc.empty())
			{if(!desc[i]->children().empty())elem.getchildKeys().push_back(desc[i]->children()[0]->value());}
			databaseFromXml.save(key_val, elem); }
	}
	catch (std::exception& ex) {std::cout << "\n  Exception occured in parsing xml back to DBElement object";std::cout << "\n  " << ex.what();}
	return databaseFromXml;
}

//Function displaying project structure and its dependency relations as an XML document
template<typename Data> std::string XmlParserDB<Data>::XmlProjectStruct() {
	std::string xml; XmlDocument doc;
	SPtr pRoot1 = makeTaggedElement("NOSQL Project Structure");
	doc.docElement() = pRoot1;
	SPtr pNameElem1 = makeTaggedElement("Convert");	pRoot1->addChild(pNameElem1);
	SPtr pTextElem1 = makeTextElement("convert.cpp");	pNameElem1->addChild(pTextElem1);
	SPtr pTextElem2 = makeTextElement("convert.h");	pNameElem1->addChild(pTextElem2);
	SPtr pNameElem2 = makeTaggedElement("CppProperties");	pRoot1->addChild(pNameElem2);
	SPtr pTextElem3 = makeTextElement("CppProperties.cpp");	pNameElem2->addChild(pTextElem3);
	SPtr pTextElem4 = makeTextElement("CppProperties.h");	pNameElem2->addChild(pTextElem4);
	SPtr pNameElem3 = makeTaggedElement("NoSql");	pRoot1->addChild(pNameElem3); SPtr pNameElem21 = makeTaggedElement("References"); pNameElem3->addChild(pNameElem21);
	SPtr pTextElem30 = makeTextElement("Convert Package"); SPtr pTextElem31 = makeTextElement("CppProperties Package"); SPtr pTextElem32 = makeTextElement("XMLDocument Package");
	pNameElem21->addChild(pTextElem30); pNameElem21->addChild(pTextElem31); pNameElem21->addChild(pTextElem32);
	SPtr pTextElem5 = makeTextElement("NoSql.cpp");	pNameElem3->addChild(pTextElem5);
	SPtr pTextElem6 = makeTextElement("NoSql.h");	pNameElem3->addChild(pTextElem6);
	SPtr pTextElem7 = makeTextElement("StrHelper.h");	pNameElem3->addChild(pTextElem7);
	SPtr pNameElem4 = makeTaggedElement("XmlDocument");	pRoot1->addChild(pNameElem4);
	SPtr pTextElem8 = makeTextElement("Tokenizer.cpp");	pNameElem4->addChild(pTextElem8);
	SPtr pTextElem9 = makeTextElement("XmlDocument.cpp");	pNameElem4->addChild(pTextElem9);
	SPtr pTextElem10 = makeTextElement("XmlElement.cpp");	pNameElem4->addChild(pTextElem10);
	SPtr pTextElem11 = makeTextElement("XmlElementParts.cpp");	pNameElem4->addChild(pTextElem11);
	SPtr pTextElem12 = makeTextElement("XmlParser.cpp");	pNameElem4->addChild(pTextElem12);
	SPtr pTextElem13 = makeTextElement("XmlWrite.cpp");	pNameElem4->addChild(pTextElem13);
	SPtr pTextElem14 = makeTextElement("XmlWriter.h");	pNameElem4->addChild(pTextElem14);
	SPtr pTextElem15 = makeTextElement("XmlParser.h");	pNameElem4->addChild(pTextElem15);
	SPtr pTextElem16 = makeTextElement("XmlElementParts.h");	pNameElem4->addChild(pTextElem16);
	SPtr pTextElem17 = makeTextElement("XmlElement.h");	pNameElem4->addChild(pTextElem17);
	SPtr pTextElem18 = makeTextElement("XmlDocument.h");	pNameElem4->addChild(pTextElem18);
	SPtr pTextElem19 = makeTextElement("Tokenizer.h");	pNameElem4->addChild(pTextElem19);
	SPtr pTextElem20 = makeTextElement("itokcollection.h");	pNameElem4->addChild(pTextElem20);
	xml = doc.toString();
	return xml;
}

//Function to Load Project Structure from XML file
template<typename Data> XmlParserDB<Data> XmlParserDB<Data>::LoadProjectStruct(std::string& xml) {
	XmlParserDB<Data> projStr;
	try {
		XmlDocument doc(xml, XmlDocument::str);
		std::vector<SPtr> desc = doc.descendents("Convert").select();
		StoreResult(desc, projStr.getconvertPackage());
		desc = doc.descendents("CppProperties").select();
		StoreResult(desc, projStr.getcppPropertyPakage());
		desc = doc.descendents("XmlDocument").select();
		StoreResult(desc, projStr.getxmlDocPackage());
		desc = doc.descendents("NoSql").select();
		projStr.getnoSQLPackage().push_back("NoSql.cpp");	projStr.getnoSQLPackage().push_back("NoSql.h");	projStr.getnoSQLPackage().push_back("StrHelper.h");
		desc = doc.descendents("References").select();
		StoreResult(desc, projStr.getchildReferences());
	}
	catch (std::exception& ex) { std::cout << "\n  " << ex.what(); }
	return projStr;
}

//Function to store the project structure retrieved from xml tags in vector
template<typename Data> void XmlParserDB<Data>::StoreResult(std::vector<SPtr> desc, std::vector<std::string>& vecRes) {
	for (std::size_t i = 0; i < desc.size(); i++) {
		if (!desc[i]->children().empty()) { vecRes.push_back(desc[i]->children()[0]->value()); }
	}
}
