/////////////////////////////////////////////////////////////////////
// NoSqlDb.cpp - key/value pair In-memory database                 //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:   NOSQL Database CSE687 Pr1, Feb-7                //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu  
/////////////////////////////////////////////////////////////////////
/*
* This file is used to demonstrate the Requirements for the NOSql Database
  Consists of functions that generate Elements, Test Queries, generate
  Current timestamp, Write to file and read from file, persist data to xml,
  load data from xml, showing queries result using functions/regex.
* 
*	Interfaces:
	std::string getTime();
	void WriteXml(std::string& xml, int param);
	std::string ReadXmlFile(int param);
	void TestingQueries(NoSqlDb<std::string> db);
	void TestTemplatebyInt(NoSqlDb<int> db2);
	void TestTempbyVector(NoSqlDb<std::vector<int>> db1);

	Build Instructions:
* -------------------
	Required files
	XmlDocument.h,XmlElement.h,XmlWriter.h

*  Version Release:
* -------------------
*	Ver 1.0
*	07-02-2017
*/
#include "NoSqlDb.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
#include "../NOSqL/XmlDocument/XmlDocument/XmlDocument.h"
#include "../NOSqL/XmlDocument/XmlElement/XmlElement.h"
#include "../NOSqL/XmlDocument/XmlWriter.h"

std::string getTime();
void WriteXml(std::string& xml, int param);
std::string ReadXmlFile(int param);
void TestingQueries(NoSqlDb<std::string> db);
void TestTemplatebyInt(NoSqlDb<int> db2);
void TestTempbyVector(NoSqlDb<std::vector<int>> db1);

	//Test Executive ---Test Stub ---
int main() {
	using StrData = std::string; 
	using Keys = NoSqlDb<StrData>::Keys;
	std::cout << "\n  Demonstrating NOSQL Database Operations \n";
	NoSqlDb<StrData> db; 
	DBElement<std::string> elem1("elem1", "test", "07-02-2017 09:12:04", "myData");
	db.save("key1", elem1);	
	DBElement<std::string> elem2("elem2", "maintain", "07-02-2017 09:13:02", "elem2's StrData");
	elem2.getchildKeys().push_back("key1");
	db.save("key2", elem2); std::cout << "\n ----Requirement 2 (Template class accepting string type data) \n" << db.value("key2").show();
	DBElement<std::string> elem3("data1", "code", getTime(), "elem3's StrData");
	elem3.getchildKeys().push_back("key2");
	db.save("key3", elem3);
	DBElement<std::string> elem4("data2", "deve", getTime(), "elem4's StrData");
	db.save("key4", elem4); Keys keys = db.keys(); 
	std::cout << "\n ----Requirement 3 (Key/Value pair added)------\n" << "size of database " << db.count();	
	for (std::string key : keys){	std::cout << "\n  " << key << ":"; std::cout << db.value(key).show();} std::cout << "\n ----Requirement 3 (Deleting Element having key 4)------\n";
	db.deletebyKey("key4"); Keys keys1 = db.keys();
	for (std::string key: keys1) { std::cout << "\n  " << key << ":"; std::cout << db.value(key).show(); } std::cout << "\n size of database after deleting " << db.count();
	std::cout << "\n ----Requirement 4 (Editing Element having key 3)------\n";
	db.editMetadata("key3","metadata1","C#","changed metadata"); std::cout << "\n ----(Requirement 4 --> Edited Metadata of key 3)--- \n" << db.value("key3").show();
	db.editChildKeys("key3","add","","key1"); std::cout << "\n ----(Requirement 4 --> Added key 1 as child key to key 3)--- \n" << db.value("key3").show();
	db.editChildKeys("key3", "delete", "key1", "key1"); std::cout << "\n ----(Requirement 4 --> Deleted key 1 child key of key 3)--- \n" << db.value("key3").show();
	DBElement<StrData> elem5("Replaced Data1", "C++", getTime(), "updated elem's StrData"); elem5.getchildKeys().push_back("key1");
	db.editbyKey("key3", elem5); std::cout << "\n ----(Requirement 4 --> Replaced value instance of key 3)--- \n" << db.value("key3").show();
	std::cout << "\n ----(Requirement 5 --> Persisting content of database at path: NoSql/myXmlFile.xml)--- \n";
	XmlParserDB<StrData> xml; std::cout << xml.PersistToDB(db);
	std::cout << "\n ----(Requirement 5 --> Loading database from path: NoSql/myXmlFile.xml)--- \n";
	NoSqlDb<StrData> dbXml = xml.LoadFromDB(ReadXmlFile(0));
	Keys keys3 = db.keys(); for (std::string key : keys3) { std::cout << "\n  " << key << ":"; std::cout << db.value(key).show(); }
	TestingQueries(db);
	NoSqlDb<std::vector<int>> db1; TestTempbyVector(db1);
	std::string projXml = xml.XmlProjectStruct();
	std::cout << "\n ----(Requirement 10 --> Project Structure stored in xml at path NOSql/projectStructure.xml)--- \n"<< projXml; WriteXml(projXml,2);
	std::cout << "\n ----(Requirement 10 --> Loading Project Structure XML from path NOSql/projectStructure.xml)--- \n" << xml.LoadProjectStruct(ReadXmlFile(2)).Display();
	std::thread t([db]() {
		XmlParserDB<StrData> xml;
		NoSqlDb<StrData> db1 = db;
		while(1){
			std::string	xmlData = xml.PersistToDB(db1); std::cout << "\n Requirement 6 --> NoSql db stored after 2 min in xml file at path NOSqL/myXmlFile.xml";
			WriteXml(xmlData, 0);
			std::this_thread::sleep_for(std::chrono::minutes(2));} });
	t.join();
}

// Function to Test Queries by Calling them in Test Executive
void TestingQueries(NoSqlDb<std::string> db) {
	std::vector<std::string> keySets; std::cout << "\n ----Requirement 7 (Testing Queries on the keysets provided by user)------\n";
	keySets.push_back("key1"); keySets.push_back("key2"); keySets.push_back("key3");
	std::cout << "\n ----Requirement 7 (Value of specified key ie: key2)----\n"; std::cout<<db.value("key2").show();
	std::cout << "\n ----Requirement 7 (Childrens of specified key ie: key1)----\n";
	std::vector<std::string> childResult=db.querychildKeys("key3");
	for (std::vector<std::string>::iterator iter = childResult.begin(); iter != childResult.end(); ++iter)
	{std::cout << *iter;}
	std::cout << "\n ----Requirement 7 (Set of all keys matching a specified pattern :: default as key)----\n";
	std::vector<std::string> defaultPatternKeys = db.getKeysfromPattern(keySets,"key");
	for (std::vector<std::string>::iterator iter1 = defaultPatternKeys.begin(); iter1 != defaultPatternKeys.end(); ++iter1)
	{std::cout << *iter1;}
	std::cout << "\n ----Requirement 7 (All keys containing specified string 'ele' in their Name )----\n";
	std::vector<std::string> itemNameKeys=db.queryItemKeys(keySets,"ele");
	for (std::vector<std::string>::iterator iter1 = itemNameKeys.begin(); iter1 != itemNameKeys.end(); ++iter1)
	{std::cout << *iter1;}
	std::cout << "\n ----Requirement 7 (All keys containing a specified pattern 'Str' in their data)----\n";
	std::vector<std::string> dataQueryKeys = db.queryDataPatternKeys(keySets, "Str");
	for (std::vector<std::string>::iterator iter1 = dataQueryKeys.begin(); iter1 != dataQueryKeys.end(); ++iter1)
	{std::cout << *iter1;}
	std::cout << "\n ----Requirement 7 (All keys whose values written within 07-02-2017 09:12:02 and 07-02-2017 09:15:04)----\n";
	std::vector<std::string> timeDateKeys = db.queryTimeDateKeys(keySets,"07-02-2017 09:12:02","07-02-2017 09:15:04");
	for (std::vector<std::string>::iterator iter1 = timeDateKeys.begin(); iter1 != timeDateKeys.end(); ++iter1)
	{std::cout << *iter1;}
	std::cout << "\n ----Requirement 7 (All keys containing a specified pattern 'tes' in their Category)----\n";
	std::vector<std::string> categoryKeys = db.queryCategoryKeys(keySets,"tes");
	for (std::vector<std::string>::iterator iter1 = categoryKeys.begin(); iter1 != categoryKeys.end(); ++iter1)
	{	std::cout << *iter1;}
	std::cout << "\n ----Requirement 8 (Queries performed on category and ItemName keysets received from earlier quries)----\n";
	std::vector<std::string> compoundQueryKeys1 = db.CompoundQuery(false, db.queryItemKeys(keySets, "elem"), db.queryCategoryKeys(keySets, "tes"));
	for (std::vector<std::string>::iterator iter1 = compoundQueryKeys1.begin(); iter1 != compoundQueryKeys1.end(); ++iter1)
	{	std::cout << *iter1;}
	std::cout << "\n ----Requirement 9 (Union of category and ItemName keysets)----\n";
	std::vector<std::string> compoundQueryKeys=db.CompoundQuery(true,db.queryItemKeys(keySets,"elem"),db.queryCategoryKeys(keySets,"tes"));
	for (std::vector<std::string>::iterator iter1 = compoundQueryKeys.begin(); iter1 != compoundQueryKeys.end(); ++iter1)
	{std::cout << *iter1;}
}

//To fulfill Requirement 2 by testing other data type
void TestTemplatebyInt(NoSqlDb<int> db2) {
	DBElement<int> itElem("intElem", "Java", getTime(), 10);
	db2.save("intkey1",itElem);
	DBElement<int> itElem1("intElem2", "python", getTime(), 25);
	itElem1.getchildKeys().push_back("intkey1");
	db2.save("intkey2", itElem1);
	std::cout << "\n ----Requirement 2 (Template class accepting Int type data) \n" << db2.value("intkey2").show();
}

//Testing Template class by using vector<int> as data type 
void TestTempbyVector(NoSqlDb<std::vector<int>> db1) {
	std::vector<int> testVec1,testVec2; 
	using Keys = NoSqlDb<std::string>::Keys;
	testVec1.push_back(13); 
	testVec2.push_back(45);
	DBElement<std::vector<int>> vecElem("vecElem1", "C++ Overload", getTime(), testVec1);
	db1.save("vecKey1",vecElem);
	DBElement<std::vector<int>> vecElem2("vecElem2", "C# Overload", getTime(), testVec2);
	vecElem2.getchildKeys().push_back("vecKey1");
	db1.save("vecKey2", vecElem2);
	std::cout << "\n ----Requirement 2 (NoSql Template Class accepting vector<int>element --> Records saved in database NOSql<Vector<int>> are ) \n";
	Keys keys = db1.keys();
	for (std::string key : keys) { std::cout << "\n  " << key << ":"; std::cout << db1.value(key).show(); }
	std::cout << "\n ----Requirement 2 & 7 (Querying the NOSql<Vector<int>> database with specific key as vecKey2) \n" << db1.value("vecKey2").show();
	XmlParserDB<std::vector<int>> xml;
	std::cout << "\n ----Requirement 5 (Persisting NOSql<Vector<int>> database at path NoSql/vectorDatabase.xml) \n";
	std::string	vecXmlData = xml.PersistToDB(db1); std::cout << vecXmlData;
	WriteXml(vecXmlData,1);
	std::cout << "\n ----Requirement 5 (Loading NOSql<Vector<int>> database from path NoSql/vectorDatabase.xml) \n";
	std::string vecXml = ReadXmlFile(1);
	NoSqlDb<std::vector<int>> db5= xml.LoadFromDB(vecXml);
	Keys keys2 = db5.keys();
	for (std::string key : keys2) { std::cout << "\n  " << key << ":"; std::cout << db5.value(key).show(); }
}

//Fuction used to get the current date time in the specific format
std::string getTime() {
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer, 80, "%d-%m-%Y %I:%M:%S", timeinfo);
	std::string timeStr(buffer);
	return timeStr;
}

//Fuction to read content from xml file
std::string ReadXmlFile(int param) {
	std::string line;
	std::string file_contents;
	if (param==0) {
		std::ifstream myfile("myXmlFile.xml");
		if (myfile.is_open())
		{	while (std::getline(myfile, line))
			{	file_contents += line;
				file_contents.push_back('\n');}
			myfile.close();}
	}
	else if (param==1) {
		std::ifstream myfile("vectorDatabase.xml");
		if (myfile.is_open())
		{while (std::getline(myfile, line))
			{	file_contents += line;
				file_contents.push_back('\n');}
			myfile.close();}
	}
	else {
		std::ifstream myfile("projectStructure.xml");
		if (myfile.is_open())
		{while (std::getline(myfile, line))
			{	file_contents += line;
				file_contents.push_back('\n');}
			myfile.close();}}
return file_contents;
}

//Fuction to write content to xml file
void WriteXml(std::string& xml,int param) {
	try {
		std::ofstream xmlFile;
		if (param == 0) {
			xmlFile.open("myXmlFile.xml");
		}
		else if (param==1) {
			xmlFile.open("vectorDatabase.xml");
		}
		else{
			xmlFile.open("projectStructure.xml");
		}
		xmlFile << xml;
		xmlFile.close();
	}
	catch (std::exception& ex) {
		std::cout << "\n  Exception occured in writing data to xml file"; std::cout << "\n  " << ex.what();
	}
}